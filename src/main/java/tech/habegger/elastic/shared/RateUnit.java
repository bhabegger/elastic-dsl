package tech.habegger.elastic.shared;

@SuppressWarnings("unused")
public enum RateUnit {
    second,
    minute,
    hour,
    day,
    week,
    month,
    quarter,
    year
}
