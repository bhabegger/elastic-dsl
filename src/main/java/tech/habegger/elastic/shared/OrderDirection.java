package tech.habegger.elastic.shared;

@SuppressWarnings("unused")
public enum OrderDirection {
    asc,
    desc
}
