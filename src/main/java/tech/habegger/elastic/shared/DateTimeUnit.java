package tech.habegger.elastic.shared;

public enum DateTimeUnit {
    year,
    month,
    day,
    hour,
    minute,
    second
}
