package tech.habegger.elastic.shared;

public record SingleFieldSpec(String field) {
}
