package tech.habegger.elastic.shared;

public enum MultiAggregationMode {
    avg,
    min,
    max,
    sum,
    median
}
