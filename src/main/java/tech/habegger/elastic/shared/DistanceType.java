package tech.habegger.elastic.shared;

@SuppressWarnings("unused")
public enum DistanceType {
    plane,
    arc
}
