package tech.habegger.elastic.shared;

@SuppressWarnings("unused")
public enum CollectMode {
    depth_first,
    breadth_first
}
