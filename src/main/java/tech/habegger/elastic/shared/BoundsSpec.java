package tech.habegger.elastic.shared;

public record BoundsSpec(
    double min,
    double max
) { }
