package tech.habegger.elastic.shared;

@SuppressWarnings("unused")
public enum TimeUnit {
    milliseconds("ms"),
    seconds("s"),
    minutes("m"),
    hours ("h"),
    days ("d");

    private final String shortSymbol;

    TimeUnit(String shortSymbol) {
        this.shortSymbol = shortSymbol;
    }

    public String quantity(int amount) {
        return "%d%s".formatted(amount, shortSymbol);
    }
}
