package tech.habegger.elastic.mapping;

public record ElasticSettings(ElasticObjectProperty mappings) {
}
