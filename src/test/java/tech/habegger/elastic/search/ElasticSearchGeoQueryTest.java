package tech.habegger.elastic.search;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static tech.habegger.elastic.TestUtils.MAPPER;
import static tech.habegger.elastic.search.ElasticBooleanClause.newBool;
import static tech.habegger.elastic.search.ElasticGeoBoundingBoxClause.geoBoundingBox;
import static tech.habegger.elastic.search.ElasticGeoDistanceClause.geoDistance;
import static tech.habegger.elastic.search.ElasticGeoGridClause.geoGrid;
import static tech.habegger.elastic.search.ElasticGeoPolygonClause.geoPolygon;
import static tech.habegger.elastic.search.ElasticGeoShapeClause.GeoInlineShapeDefinition.geoEnvelop;
import static tech.habegger.elastic.search.ElasticGeoShapeClause.GeoShapeRelation.within;
import static tech.habegger.elastic.search.ElasticGeoShapeClause.geoShape;
import static tech.habegger.elastic.search.ElasticMatchAllClause.matchAll;
import static tech.habegger.elastic.shared.FieldInstanceReference.dataPoint;
import static tech.habegger.elastic.shared.GeoCoord.geoCoord;

class ElasticSearchGeoQueryTest {


    @Test
    void geoGridQuery() throws JsonProcessingException {
        // Given
        var query = ElasticSearchRequest.query(
            geoGrid("location", "u0mhhvh3tvu")
        );

        // When
        var actual = MAPPER.writeValueAsString(query);

        // Then
        assertThat(actual).isEqualToIgnoringWhitespace(
    """
            {
                "query": {
                    "geo_grid": {
                        "location": {
                            "geohash": "u0mhhvh3tvu"
                        }
                    }
                }
            }
            """
        );
    }

    @Test
    void geoBoundingBoxQuery() throws JsonProcessingException {
        // Given
        var query = ElasticSearchRequest.query(
                newBool()
                    .must(matchAll())
                    .filter(
                        geoBoundingBox(
                        "pin.location",
                            geoCoord(40.73, -74.1),
                            geoCoord(40.01, -71.12)
                        )
                    )
                .build()
        );

        // When
        var actual = MAPPER.writeValueAsString(query);

        // Then
        assertThat(actual).isEqualToIgnoringWhitespace(
    """
            {
              "query": {
                "bool": {
                  "must": [
                    {
                        "match_all": {}
                    }
                  ],
                  "filter": [{
                    "geo_bounding_box": {
                      "pin.location": {
                        "top_left": {
                          "lat": 40.73,
                          "lon": -74.1
                        },
                        "bottom_right": {
                          "lat": 40.01,
                          "lon": -71.12
                        }
                      }
                    }
                  }]
                }
              }
            }
            """
        );
    }

    @Test
    void geoDistanceQuery() throws JsonProcessingException {
        // Given
        var query = ElasticSearchRequest.query(
                newBool()
                    .must(matchAll())
                    .filter(
                            geoDistance(
                                    "pin.location",
                                    "200km",
                                    geoCoord(40f, -70f)
                            )
                    )
                    .build()
        );

        // When
        var actual = MAPPER.writeValueAsString(query);

        // Then
        assertThat(actual).isEqualToIgnoringWhitespace(
    """
            {
              "query": {
                "bool": {
                  "must": [{
                    "match_all": {}
                  }],
                  "filter": [{
                    "geo_distance": {
                      "distance": "200km",
                      "pin.location": {
                        "lat": 40.0,
                        "lon": -70.0
                      }
                    }
                  }]
                }
              }
            }
            """
        );
    }

    @Test
    void geoPolygonQuery() throws JsonProcessingException {
        // Given
        var query = ElasticSearchRequest.query(
                newBool()
                        .must(matchAll())
                        .filter(
                            geoPolygon(
                                "person.location",
                                geoCoord(40f, -70f),
                                geoCoord(30f, -80f),
                                geoCoord(20f, -90f)
                            )
                        )
                        .build()
        );

        // When
        var actual = MAPPER.writeValueAsString(query);

        // Then
        assertThat(actual).isEqualToIgnoringWhitespace(
                """
                        {
                           "query": {
                             "bool": {
                               "must": [{
                                 "match_all": {}
                               }],
                               "filter": [{
                                 "geo_polygon": {
                                   "person.location": {
                                     "points": [
                                       { "lat": 40.0, "lon": -70.0 },
                                       { "lat": 30.0, "lon": -80.0 },
                                       { "lat": 20.0, "lon": -90.0 }
                                     ]
                                   }
                                 }
                               }]
                             }
                           }
                         }
                        """
        );
    }

    @Test
    void geoShapeInlineQuery() throws JsonProcessingException {
        // Given
        var query = ElasticSearchRequest.query(
                newBool()
                    .must(matchAll())
                    .filter(
                        geoShape(
                        "location",
                            within,
                            geoEnvelop(
                                geoCoord(13f, 53f),
                                geoCoord(14f, 52f)
                            )
                        )
                    )
                .build()
        );

        // When
        var actual = MAPPER.writeValueAsString(query);

        // Then
        assertThat(actual).isEqualToIgnoringWhitespace(
    """
            {
              "query": {
                "bool": {
                  "must": [{
                    "match_all": {}
                  }],
                  "filter": [{
                    "geo_shape": {
                      "location": {
                        "shape": {
                          "type": "envelope",
                          "coordinates": [ [ 13.0, 53.0 ], [ 14.0, 52.0 ] ]
                        },
                        "relation": "within"
                      }
                    }
                  }]
                }
              }
            }
            """
        );
    }

    @Test
    void geoShapeIndexQuery() throws JsonProcessingException {
        // Given
        var query = ElasticSearchRequest.query(
            newBool()
                .filter(
                    geoShape(
                    "location",
                        dataPoint("shapes", "deu", "location")
                    )
                )
                .build()
        );

        // When
        var actual = MAPPER.writeValueAsString(query);

        // Then
        assertThat(actual).isEqualToIgnoringWhitespace(
    """
            {
              "query": {
                "bool": {
                  "filter": [{
                    "geo_shape": {
                      "location": {
                        "indexed_shape": {
                          "index": "shapes",
                          "id": "deu",
                          "path": "location"
                        }
                      }
                    }
                  }]
                }
              }
            }
            """
        );
    }
}